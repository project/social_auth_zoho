# Social Auth Zoho

Social Auth GitHub is a GitHub authentication integration for
Drupal. It is based on the Social Auth and Social API projects

It adds to the site:

- A new url: `/user/login/zoho`.
- A settings form at `/admin/config/social-api/social-auth/zoho`.
- A GitHub logo in the Social Auth Login block.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/social_auth_zoho).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/social_auth_zoho).


## Table of contents

- Requirements
- Installation
- Configuration
- How it works
- Support requests
- Maintainers


## Requirements

This module requires the following modules:

- [Social Auth](https://drupal.org/project/social_auth)
- [Social API](https://drupal.org/project/social_api)


## Installation

- Run composer to install the dependencies.
   composer require "`drupal/social_auth_zoho`"

- Install the dependencies: Social API and Social Auth.

- Install as you would normally install a contributed Drupal module. For further
  information, see
  [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Add your Zoho project OAuth information in
   Configuration » User Authentication » Zoho.
2. Place a Social Auth Login block in Structure » Block Layout.
3. If you already have a Social Auth Login block in the site, rebuild the cache.


## How it works

The user can click on the GitHub logo in the Social Auth Login block.
You can also add a button or link anywhere on the site that points
to `/user/login/zoho`, so theming and customizing the button or link
is very flexible.

After GitHub has returned the user to your site, the module compares the
user id or email address provided by Z. If the user has previously
registered using GitHub or your site already has an account with the same
email address, the user is logged in. If not, a new user account is created.
Also, a GitHub account can be associated with an authenticated user.


## Support requests

- Before posting a support request, carefully read the installation
  instructions provided in module documentation page.

- Before posting a support request, check the Recent Log entries at
  admin/reports/dblog

- Once you have done this, you can post a support request at module issue
  queue: `https://www.drupal.org/project/issues/social_auth_github`

- When posting a support request, please inform if you were able to see any
  errors in the Recent Log entries.


## Maintainers 

- Talla Subbarao - [phpsubbarao](https://www.drupal.org/u/phpsubbarao)
- Mallikarjuna Gouda - [mallikarjuna013](https://www.drupal.org/u/mallikarjuna013)
- Matthew Lechleider - [Slurpee](https://www.drupal.org/u/slurpee)
- Pragya Diwan - [pragya.diwan](https://www.drupal.org/u/pragyadiwan)

 * [Subbarao Talla (Talla)](https://www.drupal.org/u/phpsubbarao)
 * [Mallikarjuna ](https://www.drupal.org/u/mallikarjuna013)
 * [Pragya Diwan](https://www.drupal.org/u/pragyadiwan)
 * [Slurpee](https://www.drupal.org/u/slurpee)

Development sponsored by:

 * [ITT Digitil](https://www.drupal.org/itt-digital)
