<?php

namespace Drupal\social_auth_zoho\Controller;

use GuzzleHttp\Client;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Controller\OAuth2ControllerBase;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_zoho\ZohoAuthManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Url;

/**
 * Returns responses for Social Auth Zoho routes.
 */
class ZohoAuthController extends OAuth2ControllerBase {

  /**
   * ZohoAuthController constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of social_auth_zoho network plugin.
   * @param \Drupal\social_auth\User\UserAuthenticator $user_authenticator
   *   Manages user login/registration.
   * @param \Drupal\social_auth_zoho\ZohoAuthManager $zoho_manager
   *   Used to manage authentication methods.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Used to access GET parameters.
   * @param \Drupal\social_auth\SocialAuthDataHandler $data_handler
   *   The Social Auth data handler.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Used to handle metadata for redirection to authentication URL.
   */
  public function __construct(MessengerInterface $messenger,
    NetworkManager $network_manager,
    UserAuthenticator $user_authenticator,
    ZohoAuthManager $zoho_manager,
    RequestStack $request,
    SocialAuthDataHandler $data_handler,
    RendererInterface $renderer) {

    parent::__construct('Social Auth Zoho', 'social_auth_zoho',
      $messenger, $network_manager, $user_authenticator,
      $zoho_manager, $request, $data_handler, $renderer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_authenticator'),
      $container->get('social_auth_zoho.manager'),
      $container->get('request_stack'),
      $container->get('social_auth.data_handler'),
      $container->get('renderer')
    );
  }

  /**
   * Response for path 'user/login/zoho/callback'.
   *
   * Zoho returns the user here after user has authenticated.
   */
  public function callback() {

    $config = $this->config('social_auth_zoho.settings');
    $postReq = \Drupal::request()->request->all();
    $accounts_server = \Drupal::request()->query->get('accounts-server');
    $code = \Drupal::request()->query->get('code');
    $client_id = $config->get('client_id');
    $client_sectet = $config->get('client_secret');
    $redirect_url = Url::fromRoute('social_auth_zoho.callback')->setAbsolute()->toString();

    $url = $accounts_server . "/oauth/v2/token?client_id=$client_id&client_secret=$client_sectet&grant_type=authorization_code&redirect_uri=" . $redirect_url . "&code=$code";

    $client = new Client();
    $res = $client->request(
      'POST',
      $url,
      [
        'headers' =>
        [
          'Content-Type' => 'application/json',
        ],
      ]
    );

    $body = json_decode($res->getBody()->getContents(), TRUE);

    $access_token = $body['access_token'];
    $tokenParts = explode(".", $body['id_token']);
    $tokenHeader = base64_decode($tokenParts[0]);
    $tokenPayload = base64_decode($tokenParts[1]);
    $jwtHeader = json_decode($tokenHeader);
    $jwtPayload = json_decode($tokenPayload);

    if (!empty($jwtPayload->email)) {
      $username = strstr($jwtPayload->email, '@', TRUE);
      ;

      return $this->userAuthenticator->authenticateUser($username,
        $jwtPayload->email,
        $jwtPayload->email,
        $access_token,
        '',
        $body);
    }

    return $this->redirect('user.login');
  }

}
