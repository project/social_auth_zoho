<?php

namespace Drupal\social_auth_zoho\Settings;

/**
 * Defines an interface for Social Auth Zoho settings.
 */
interface ZohoAuthSettingsInterface {

  /**
   * Gets the client ID.
   *
   * @return string
   *   The client ID.
   */
  public function getClientId();

  /**
   * Gets the client secret.
   *
   * @return string
   *   The client secret.
   */
  public function getClientSecret();

}
