<?php

namespace Drupal\social_auth_zoho\Plugin\Network;

use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Defines the Zoho Auth interface.
 */
interface ZohoAuthInterface extends NetworkInterface {}
