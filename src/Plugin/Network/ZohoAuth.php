<?php

namespace Drupal\social_auth_zoho\Plugin\Network;

use Drupal\Core\Url;
use Drupal\social_api\SocialApiException;
use Drupal\social_auth\Plugin\Network\NetworkBase;
use Drupal\social_auth_zoho\Settings\ZohoAuthSettings;
// Use League\OAuth2\Client\Provider\Github;.
use Asad\OAuth2\Client\Provider\Zoho;

/**
 * Defines a Network Plugin for Social Auth Zoho.
 *
 * @package Drupal\social_auth_zoho\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_zoho",
 *   social_network = "Zoho",
 *   type = "social_auth",
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_zoho\Settings\ZohoAuthSettings",
 *       "config_id": "social_auth_zoho.settings"
 *     }
 *   }
 * )
 */
class ZohoAuth extends NetworkBase implements ZohoAuthInterface {

  /**
   * Sets the underlying SDK library.
   *
   * @return \League\OAuth2\Client\Provider\Zoho|false
   *   The initialized 3rd party library instance.
   *   False if library could not be initialized.
   *
   * @throws \Drupal\social_api\SocialApiException
   *   If the SDK library does not exist.
   */
  protected function initSdk() {

    $class_name = '\Asad\OAuth2\Client\Provider\Zoho';
    if (!class_exists($class_name)) {
      throw new SocialApiException(sprintf('The Zoho library for PHP League OAuth2 not found. Class: %s.', $class_name));
    }

    /** @var \Drupal\social_auth_zoho\Settings\ZohoAuthSettings $settings */
    $settings = $this->settings;

    if ($this->validateConfig($settings)) {
      // All these settings are mandatory.
      $league_settings = [
        'clientId' => $settings->getClientId(),
        'clientSecret' => $settings->getClientSecret(),
        'redirectUri' => Url::fromRoute('social_auth_zoho.callback')->setAbsolute()->toString(),
      ];

      return new Zoho($league_settings);
    }

    return FALSE;
  }

  /**
   * Checks that module is configured.
   *
   * @param \Drupal\social_auth_zoho\Settings\ZohoAuthSettings $settings
   *   The Zoho auth settings.
   *
   * @return bool
   *   True if module is configured.
   *   False otherwise.
   */
  protected function validateConfig(ZohoAuthSettings $settings) {
    $client_id = $settings->getClientId();
    $client_secret = $settings->getClientSecret();
    if (!$client_id || !$client_secret) {
      $this->loggerFactory
        ->get('social_auth_zoho')
        ->error('Define Client ID and Client Secret on module settings.');

      return FALSE;
    }

    return TRUE;
  }

}
